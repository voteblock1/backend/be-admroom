import { Schema, Document, model } from 'mongoose';

let proposalSchema = new Schema({
    description: {
        type: String,
    },
    state: {
        type: Boolean,
        default: true
    },
    idCandidate: { 
        type: Schema.Types.ObjectId, 
        ref: 'Candidate',
        required: true
    }
},
{
    timestamps: true
});


interface IProposal extends Document {
    description: string;
    state: boolean;
    idCandidate: string;
}

export default model<IProposal>('Proposal', proposalSchema);