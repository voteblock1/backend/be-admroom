import { Schema, Document, model } from 'mongoose';
import { environment } from '../../environments/environment';

let roomSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
    },
    startTime: {
        type: Date,
        required: [true, 'The startTime is necessary'],
    },
    endTime: {
        type: Date,
        required: [true, 'The endTime is necessary'],
    },
    description: {
        type: String,
    },
    image: {
        type: String,
        required: false,
        default: `${environment.HOST}uploads/room/image/defaultRoom.png`
    },
    loginCode: {
        type: String,
        required: [true, 'The loginCode is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    },
    numberPlaces: {
        type: Number,
        default: 3
    },
    idTypeVote: { 
        type: Schema.Types.ObjectId, 
        ref: 'TypeVote',
        required: true
    },
    idUser: {
        type: String,
        required: true,
    },
    idCompany: {
        type: String,
        required: true,
    }
},
{
    timestamps: true
});


interface IRoom extends Document {
    name: string;
    startTime: Date;
    endTime: Date;
    description: string;
    image: string;
    loginCode: string;
    state: boolean;
    numberPlaces: number;
    idTypeVote: string;
    idUser: string;
}

export default model<IRoom>('Room', roomSchema);