import { Schema, Document, model } from 'mongoose';

let typeVoteSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
        unique: true
    },
    state: {
        type: Boolean,
        default: true
    },
    description: {
        type: String,
    }
},
{
    timestamps: true
});

interface ITypeVote extends Document {
    name: string;
    state: boolean;
    description: string;
}

export default model<ITypeVote>('TypeVote', typeVoteSchema);