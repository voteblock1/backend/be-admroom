import { Schema, Document, model } from 'mongoose';
import { environment } from '../../environments/environment';

let candidateSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
    },
    description: {
        type: String,
    },
    state: {
        type: Boolean,
        default: true
    },
    idRoom: { 
        type: Schema.Types.ObjectId, 
        ref: 'Room',
        required: true
    },
    image: {
        type: String,
        required: false,
        default: `${environment.HOST}uploads/candidate/image/defaultCandidate.png`
    },
},
{
    timestamps: true
});


interface ICandidate extends Document {
    name: string;
    description: string;
    image: string;
    state: boolean;
    idRoom: string;
}

export default model<ICandidate>('Candidate', candidateSchema);