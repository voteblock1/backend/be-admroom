import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Proposal from '../models/proposal.model';
const CProposal = Router();
const authentication = new Authentication();


/**
 * @description Method for creating proposal.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.post( '/proposal', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let proposal = new Proposal({
        description: body.description,
        idCandidate: body.idCandidate,
    });
    proposal.save((err, proposal) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: proposal
        });
    });
});


/**
 * @description Method to get all proposal.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.get('/proposal', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Proposal.find({state: true})
            .skip(from)
            .limit(limit)
            .populate('idCandidate', 'name')
            .exec((err, proposal) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Proposal.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        proposal,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get proposal by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.get('/proposal/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error proposal doesn't exist"});
    }

    Proposal.findById(id, (err, proposal) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! proposal) return res.status(404).json({message: "Error proposal doesn't exist"});

            return res.status(200).json({
                ok: true,
                proposal
            });
        });
});

/**
 * @description Method that updates the attributes of a proposal.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.put('/proposal/:id', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, [
        'description',
    ]);

    Proposal.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, proposal) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! proposal) return res.status(404).json({message: "Error proposal doesn't exist"});

        res.json({
            ok: true,
            user: proposal
        });
    });
});

/**
 * @description Method that delete a proposal.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.delete('/proposal/:id', [ authentication.buildCheckToken() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Proposal.findByIdAndUpdate( id, changeState, {new: true}, (err, proposal) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(proposal === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'proposal not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: proposal
            });
    });
});

/**
 * @description return all proposal for idCandidate.
 * @date 04/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CProposal.get('/proposal/candidate/:idCandidate', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let idCandidate = req.params.idCandidate;
    Proposal.find({idCandidate: idCandidate, state: true})
        .populate('idCandidate', 'name')
        .exec((err, proposal) => {
            if(err) {return res.status(500).json({message: 'Error returning data'});}

            return res.status(200).json({
                ok: true,
                proposal
            });
        });
});

export default CProposal;