import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Room from '../models/room.model';
const CRoom = Router();
const authentication = new Authentication();
import Multer from '../classes/Multer.class';
import { environment } from '../../environments/environment';
import { Mongoose } from 'mongoose';
const multer = new Multer();


/**
 * @description Method for creating room.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.post( '/room', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let room = new Room({
        name: body.name,
        startTime: body.startTime,
        endTime: body.endTime,
        description: body.description,
        loginCode: body.loginCode,
        numberPlaces: body.numberPlaces,
        idTypeVote: body.idTypeVote,
        idUser: body.idUser,
        idCompany: body.idCompany,
    });
    room.save((err, room) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            room: room
        });
    });
});


/**
 * @description Method to get all room.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.get('/room', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Room.find({state: true})
            .skip(from)
            .limit(limit)
            .populate('idTypeVote', 'name')
            .exec((err, room) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Room.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        room,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get room by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.get('/room/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error room doesn't exist"});
    }

    Room.findById(id, (err, room) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! room) return res.status(404).json({message: "Error room doesn't exist"});

            return res.status(200).json({
                ok: true,
                room
            });
        });
});


/**
 * @description Search for rooms by company.
 * @date 02/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.get('/room/search/:item/:idCompany', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let item = req.params.item;
    let idCompany = req.params.idCompany;
    let regex = new RegExp(item, 'i');
    Room.find({name: regex, state: true, idCompany: idCompany})
        .populate('idTypeVote', 'name')
        .exec((err, room) => {
            if(err) {return res.status(500).json({message: 'Error returning data'});}

            return res.status(200).json({
                ok: true,
                room
            });
        });
});


/**
 * @description Method that updates the attributes of a room.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.put('/room/:id', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, [
        'name', 
        'description',
        'startTime',
        'endTime',
        'loginCode',
        'numberPlaces',
    ]);

    Room.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, room) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! room) return res.status(404).json({message: "Error room doesn't exist"});

        res.json({
            ok: true,
            user: room
        });
    });
});

/**
 * @description Method that delete a room.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.delete('/room/:id', [ authentication.buildCheckToken() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Room.findByIdAndUpdate( id, changeState, {new: true}, (err, room) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(room === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'room not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: room
            });
    });
});

/**
 * @description Method to get all room of one company.
 * @date 31/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.get('/room/idComany/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;

    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Room.find({state: true, idCompany: id})
            .skip(from)
            .limit(limit)
            .populate('idTypeVote', 'name')
            .exec((err, room) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                if(room.length === 0) { 
                    return res.status(404).json({message: "Error room doesn't exist for this company"});
                }

                Room.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        room,
                        howMany: count
                    });
                });
            });
});


/**
 * @description Method that upload images for room.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.post( '/room/upload-image/:id', [ 
    /*authentication.buildCheckToken(), */
    multer.saveImageServer('room').single('image'),
    /*authentication.buildCheckAdminRole()*/ ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let pathImage = req.file.path.split('public/');

    let uploadImage = {
        image: `${environment.HOST}${pathImage[1]}`
    };
    Room.findByIdAndUpdate( id, uploadImage, {new: true}, (err, roomDB) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(roomDB === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Room not found'
                    }
                });
            }
            res.json({
                ok: true,
                room: roomDB
            });
    });
});


/**
 * @description Search for rooms by company.
 * @date 02/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.get('/room/enter/:idRoom/:code', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let idRoom = req.params.idRoom;
    let code = req.params.code;
    Room.find({_id: idRoom, state: true, loginCode: code})
        .populate('idTypeVote', 'name')
        .exec((err, room) => {
            if(err) {return res.status(500).json({message: 'Error returning data'});}

            return res.status(200).json({
                ok: true,
                room
            });
        });
});


/**
 * @description Get rooms for voter by Array of ids.
 * @date 04/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CRoom.post('/room/array', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let body = req.body;
    let listRooms = body.arrayList.map((e: any) => e.idRoom);

    Room.find({_id: {$in: listRooms}})
        .populate('idTypeVote', 'name')
        .exec((err, room) => {
            if(err) {return res.status(500).json({message: 'Error returning data'});}

            return res.status(200).json({
                ok: true,
                room
            });
        });
});

export default CRoom;