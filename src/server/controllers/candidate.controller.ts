import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Candidate from '../models/candidate.model';
const CCandidate = Router();
const authentication = new Authentication();
import Multer from '../classes/Multer.class';
import { environment } from '../../environments/environment';
const multer = new Multer();


/**
 * @description Method for creating candidate.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.post( '/candidate', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let candidate = new Candidate({
        name: body.name,
        description: body.description,
        idRoom: body.idRoom,
    });
    candidate.save((err, candidate) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: candidate
        });
    });
});


/**
 * @description Method to get all candidate.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.get('/candidate', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Candidate.find({state: true})
            .skip(from)
            .limit(limit)
            .populate('idRoom', 'name')
            .exec((err, candidate) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Candidate.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        candidate,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get candidate by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.get('/candidate/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error candidate doesn't exist"});
    }

    Candidate.findById(id, (err, candidate) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! candidate) return res.status(404).json({message: "Error candidate doesn't exist"});

            return res.status(200).json({
                ok: true,
                candidate
            });
        });
});

/**
 * @description Method that updates the attributes of a candidate.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.put('/candidate/:id', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, [
        'name', 
        'description',
    ]);

    Candidate.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, candidate) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! candidate) return res.status(404).json({message: "Error candidate doesn't exist"});

        res.json({
            ok: true,
            user: candidate
        });
    });
});

/**
 * @description Method that delete a candidate.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.delete('/candidate/:id', [ authentication.buildCheckToken() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Candidate.findByIdAndUpdate( id, changeState, {new: true}, (err, candidate) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(candidate === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'candidate not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: candidate
            });
    });
});


/**
 * @description Method that upload images for candidate.
 * @date 02/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.post( '/candidate/upload-image/:id', [ 
    /*authentication.buildCheckToken(), */
    multer.saveImageServer('candidate').single('image'),
    /*authentication.buildCheckAdminRole()*/ ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let pathImage = req.file.path.split('public/');

    let uploadImage = {
        image: `${environment.HOST}${pathImage[1]}`
    };
    Candidate.findByIdAndUpdate( id, uploadImage, {new: true}, (err, candidateDB) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(candidateDB === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Candidate not found'
                    }
                });
            }
            res.json({
                ok: true,
                candidate: candidateDB
            });
    });
});

/**
 * @description Search for rooms by company.
 * @date 02/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCandidate.get('/candidate/room/:idRoom', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let idRoom = req.params.idRoom;
    Candidate.find({idRoom: idRoom, state: true})
        .populate('idTypeVote', 'name')
        .exec((err, room) => {
            if(err) {return res.status(500).json({message: 'Error returning data'});}

            return res.status(200).json({
                ok: true,
                room
            });
        });
});
export default CCandidate;