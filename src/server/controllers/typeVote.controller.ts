import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import TypeVote from '../models/typeVote.model';
const CTypeVote = Router();
const authentication = new Authentication();


/**
 * @description Method for creating typeVote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.post( '/typeVote', [ authentication.buildCheckToken(), authentication.checkSuperAdmAndAdmRol() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let typeVote = new TypeVote({
        name: body.name,
        description: body.description,
    });
    typeVote.save((err, typeVoteDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: typeVoteDB
        });
    });
});


/**
 * @description Method to get all typeVote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.get('/typeVote', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    TypeVote.find({state: true})
            .skip(from)
            .limit(limit)
            .exec((err, typeVote) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                TypeVote.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        typeVote,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get typeVote by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.get('/typeVote/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error typeVote doesn't exist"});
    }

    TypeVote.findById(id, (err, typeVote) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! typeVote) return res.status(404).json({message: "Error typeVote doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeVote
            });
        });
});

/**
 * @description Method that updates the attributes of a typeVote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.put('/typeVote/:id', [ authentication.buildCheckToken(), authentication.checkSuperAdmAndAdmRol() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, ['name', 'description']);

    TypeVote.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, typeVote) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! typeVote) return res.status(404).json({message: "Error typeVote doesn't exist"});

        res.json({
            ok: true,
            user: typeVote
        });
    });
});

/**
 * @description Method that delete a typeVote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.delete('/typeVote/:id', [ authentication.buildCheckToken(), authentication.checkSuperAdmAndAdmRol() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    TypeVote.findByIdAndUpdate( id, changeState, {new: true}, (err, typeVote) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(typeVote === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'typeVote not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: typeVote
            });
    });
});


/**
 * @description Method to get typeVote by name.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeVote.post( '/typeVote/name', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;
    TypeVote.findOne({name: body.name}, (err, typeVote) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! typeVote ) return res.status(404).json({message: "Error typeVote doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeVote
            });
        });
});

export default CTypeVote;