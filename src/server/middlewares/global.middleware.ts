import express = require('express');
import bodyParser = require('body-parser');
import path = require('path');
import morgan = require('morgan');


export default class Global {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGlobal();
        this.publicFolder();
    }

    /**
     * @description Global middlewares settings.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
     private configGlobal(): void {
        this.app.use( bodyParser.urlencoded( {extended:false} ) );
        this.app.use( bodyParser.json() );
        this.app.use( morgan('dev') );
    }

    /**
     * @description Public folder settings.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    private publicFolder(): void {
        const publicPath = path.resolve(__dirname, '../../public');
        this.app.use( express.static( publicPath ));
    }

    /**
     * @description Return the settings for express to settings index.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    public getApp(): any {
        return this.app;
    }
}