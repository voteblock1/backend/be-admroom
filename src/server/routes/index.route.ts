import express = require('express');
import controllerTypeVote from '../controllers/typeVote.controller';
import controllerRoom from '../controllers/room.controller';
import controllerCandidate from '../controllers/candidate.controller';
import controllerProposal from '../controllers/proposal.controller';

export default class Routes {
    private app: express.Application;

    constructor(){
        this.app = express();
        this.loadRoutes();
    }

    private loadRoutes(): void {
        this.app.use( controllerTypeVote );
        this.app.use( controllerRoom );
        this.app.use( controllerCandidate );
        this.app.use( controllerProposal );
    }

    public getRoutes(): any {
        return this.app;
    }
}